const colors = {
  mainColor: "#3f3d38",
  darkMainColor: "#3f3d38",
  green: "#828B75",
  brown: "#AFA092",
  brandYellow: "#D8DE00",
  black: "#000000",
  red: "#FA4238",
  white: "#FFFFFF",
  gray: "#EFEFEF",
  darkGray: "#D0D0D0",
  highlight: "#FFA243"
}

export { colors };